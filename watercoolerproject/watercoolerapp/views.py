from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import UserProfileForm, PostForm, LikeForm
from django.contrib.auth import authenticate, login
from django.views import generic
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView
from django.contrib.auth.models import User
from .models import Post, Like

def index(request):
    if request.user.is_authenticated:
    	return redirect('watercooler:feed')
    else:
    	return render(request, 'watercooler/index.html')    	

def feed(request):
	if request.user.is_authenticated:
		posts = Post.objects.order_by('-published_date')
		if request.method=='POST':
			if 'Like' in request.POST:
				form = LikeForm(request.POST)
				if form.is_valid():
					data=form.save(commit=False)
					data.author = request.user
					data.save()
			else:
				form = PostForm(request.POST)
				if form.is_valid():
					data=form.save(commit=False)
					data.author = request.user
					data.save()
	else:
		return render(request, 'watercooler/index.html')
	return render(request, 'watercooler/feed.html', {'postlist':posts, 'userlist':User.objects.all()})


def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			login(request, user)
			return redirect('watercooler:signup2')
	else:
		form = UserCreationForm()
	return render(request, 'watercooler/signup.html', {'form':form})

def signup2(request):
	if request.method == 'POST':
		form = UserProfileForm(request.POST)
		if form.is_valid():
			profile=form.save(commit=False)
			profile.user=request.user
			profile.save()
			return redirect('watercooler:index')
	else:
		form = UserProfileForm()
	return render(request, 'watercooler/signup2.html',{'form':form})

