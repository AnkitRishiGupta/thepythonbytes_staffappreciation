from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile_no = models.IntegerField(blank=True, null=True)
    #company = models.ForeignKey(Company, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True, blank=True)
    anniversary_date = models.DateField(null=True, blank=True)
    profile_pic = models.ImageField(upload_to=None,height_field=None,width_field=None)

class Post(models.Model):
	author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
	content = models.TextField(max_length=200, null=True, blank=True)
	published_date = models.DateTimeField(auto_now=True)
	image = models.ImageField(null=True, blank=True)

class Like(models.Model):
	author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
	post = models.ForeignKey(Post, on_delete=models.CASCADE)

#class Company(models.Model):
	#company_name = models.CharField(max_length=30, blank=False, null=False)
