from django.urls import path
from django.contrib.auth import views as auth_views
from .import views

app_name = 'watercooler'

urlpatterns=[
	path('login/', auth_views.login, name='login'),
	path('logout/', auth_views.logout, {'next_page':'/'}, name='logout'),
	path('signup/', views.signup, name='register'),
	path('signup2/', views.signup2, name='signup2'),
	path('feed/', views.feed, name='feed'),
	path('', views.index, name='index'),
]