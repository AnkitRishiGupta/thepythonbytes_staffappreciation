from django import forms
from .models import Profile, Post, Like

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('mobile_no', 'anniversary_date', 'birth_date')		

class PostForm(forms.ModelForm):
	class Meta:
		model=Post
		fields = ('content', 'image')

class LikeForm(forms.ModelForm):
	class Meta:
		model=Like
		fields = ()