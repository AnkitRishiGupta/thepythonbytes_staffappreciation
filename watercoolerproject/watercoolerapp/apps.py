from django.apps import AppConfig


class WatercoolerappConfig(AppConfig):
    name = 'watercoolerapp'
